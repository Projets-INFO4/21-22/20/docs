# Extensions
## engine.cfg -> project.godot 
- The export to Godot 3 function (WIP) does it. The syntax is the same. Only some variables change.
## Assets folder do not change
- The export function already moves this folder without one file. So we need to move the file named `dst.crt` in the same place.
## .xml -> .tres
- Open `.xml` in godot 2 and save as `.tres`.
