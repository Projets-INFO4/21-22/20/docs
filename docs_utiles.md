# Documentation about the godot 2 -> godot 3 conversion
## TOF 3.2.2 (doesn't fully work)
- https://github.com/qarmin/Tanks-of-Freedom/tree/godot3port
## TOF 3.X manual conversion (not finished | doesn't work)
- https://github.com/PhilipLudington/Tanks-of-Freedom-Convert-to-Godot-3
## GDScript changes
- https://godotengine.org/qa/11570/will-gdscript-change-in-godot-3-0
## Signal names / Audio Engine and others changes
- https://github.com/dploeger/godot-migrationnotes
## .tscn files
- https://www.reddit.com/r/godot/comments/7rtk4g/dumb_question_how_do_i_convert_my_godot_2_project/
 For example, in .tscn files, you have to change rotation_deg to rotation_degrees, and the actual rotational value should be changed to the opposite (e.g. 10.0 should be -10.0) since Godot 3.0 changed the the sign of 2D rotations.

## A working conversion (feasible but far from easy)
- https://steemit.com/games/@cloudif/crash-replace-repeat-porting-resolutiion-to-godot-3
- https://www.monolithofminds.com/ (contains contact information of the developpers)

## List of games created with godot
- https://github.com/qarmin/Games-created-in-Godot

## Android bluetooth plugin for two devices
- https://github.com/DisDoh/GodotBluetooth

## Conversion tool (not for GDScript but still a good addition)
- https://github.com/returntocorp/pfff

## Godot 3.3 Bluetooth
- https://github.com/DisDoh/GodotBluetooth
- https://github.com/drentsoft/GodotBluetooth

## Godot 3 WebSocket documentation
- https://docs.godotengine.org/fr/stable/tutorials/networking/websocket.html