# Tanks Of Freedom multiplayer project
- Thomas ABECASSIS / Thomas FOURNIER / Lucas ZAFFUTO

Git with code: https://gricad-gitlab.univ-grenoble-alpes.fr/zaffutol/projets-info4-21-22-20-code

## Progress log
### 17/01/2022
- Installation of Godot and first reading of the code.
- First attempt to compile the game.
- Installation of the required compilation tools.
- Installation and setup of Android Studio paired with an Android emulator.
- Analysis of the Android game Frozen Bubble in an attempt to use it as an example.
### 24/01/2022
- The game can only be compiled in Godot 2.
- Installation of the correct compiling tool since they are version specific.
- Game can be compiled, runs and can be modified.
- Frozen Bubble can't be used as an example : Android Studio project coded in Java whereas TOF is a native Godot project in GDScript (specific to Godot).
- Research on how to communicate remotely using an application made with Godot.
### 31/01/2022
- Research on how to create a Wi-Fi/Bluetooth with Godot 2.
- Wi-Fi communication seems feasible in Godot 2 but is badly documented.
- Godot only allows support for Android plugins since version 3. We begin to think about converting the project in Godot 3.
- Start of research on the difference between Godot 2 and 3. The port seems discouraging so we continue to look for an alternative in Godot 2. 
- Successful import of the Android plugin in Godot 3. However, not of great interest without the game and moreover the plugin does not necessarily support communication between 2 Android devices (basically for Android/Arduino).

### 07/01/2022
- Decision to convert the game to the recent version of Godot (3) because it has way more documentation.
- We tried several ways to convert the game to Godot 3 (see other branches on git).
- 2 conversion methods found: a git supposed to work in Godot 3 directly (game that launches but broken and not fully functional) and a manual conversion guide which involves modifying the game files by hand (no conclusive launch on this side).
- Discussion on the possibility of combining these 2 conversions but a method that seems flawed and with little chance of leading to a conclusive result.

### 14/01/2022
- Discussion with M. Palix
- Conclusion that the game must be cnoverted to Godot 3.
- Detailed review of the problems related to the conversion and the lack of documentation or official tool concerning the process.
- Decision to change the main objective of the project: To produce documentation on the conversion of a Godot 2 project to Godot 3.
- This new goal involves turning our attention to Godot's code rather than the game in order to understand the differences between the versions in order to know how to adapt to them.
- Optional secondary objective: If the conversion is done, as we have already proven that the Android plugin exists in Godot 3, we can use a small communication application to prove that the concept of an online game mode on Tanks of Freedom is then feasible.

### 28/01/2022
- Mid-term presentation.
- Discussion on the new strategy to adopt.

### 07/03/2022
- Compiling of all documentation found so far in a single file.
- Translation of this file.

### 08/03/2022
- More work done on compiling and documentation

### 14/03/2022
- Documentation work is put aside
- Start of a simple Bluetooth communication project on Godot 3

### 15/03/2022
- Problems to compile an app using the Godot 3 Bluetooth android plugin
- The plugin should work but would require a considerable ammount of time to get it to actually compile

### 21/03/2022
- Decision to put aside the Bluetooth for now
- We searched for other ways to establish communication using Godot that would only use built-in librairies

### 22/03/2022
- Start of a test project using WebSockets
- Can already create a server and allow clients to connect to this server